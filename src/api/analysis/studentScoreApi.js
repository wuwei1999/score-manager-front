import request from '@/utils/request';
//查询成绩图表数据
export function listEchartsData(query) {
    return request({
        url: '/analysis/student_score/echarts_list',
        method: 'get',
        params: query,
    });
}
