import request from '@/utils/request';

// 查询考试列表
export function listExam(query) {
  return request({
    url: '/school/exam/list',
    method: 'get',
    params: query,
  });
}

// 查询考试详细
export function getExam(examId) {
  return request({
    url: `/school/exam/${examId}`,
    method: 'get',
  });
}

// 新增考试
export function addExam(data) {
  return request({
    url: '/school/exam',
    method: 'post',
    data,
  });
}

// 修改考试
export function updateExam(data) {
  return request({
    url: '/school/exam',
    method: 'put',
    data,
  });
}

// 删除考试
export function deleteExam(examId) {
  return request({
    url: `/school/exam/${examId}`,
    method: 'delete',
  });
}
