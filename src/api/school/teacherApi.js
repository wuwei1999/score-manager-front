import request from '@/utils/request';

// 查询教师列表
export function listTeacher(query) {
  return request({
    url: '/school/teacher/list',
    method: 'get',
    params: query,
  });
}

// 查询教师详细
export function getTeacher(teacherId) {
  return request({
    url: `/school/teacher/${teacherId}`,
    method: 'get',
  });
}

// 新增教师
export function addTeacher(data) {
  return request({
    url: '/school/teacher',
    method: 'post',
    data,
  });
}

// 修改教师
export function updateTeacher(data) {
  return request({
    url: '/school/teacher',
    method: 'put',
    data,
  });
}

// 删除教师
export function deleteTeacher(teacherId) {
  return request({
    url: `/school/teacher/${teacherId}`,
    method: 'delete',
  });
}

// 用户密码重置
export function resetTeacherPassword(teacherId, password) {
  const data = {
    teacherId,
    password,
  };
  return request({
    url: `/school/teacher/${teacherId}/password/reset`,
    method: 'put',
    data,
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/file/upload',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    method: 'post',
    data,
  });
}

