import request from '@/utils/request';

// 首页数据
export function list() {
    return request({
        url: '/school/clazz_up/list',
        method: 'get',
    });
}
// 升班操作
export function clazzUp(data) {
    return request({
        url: '/school/clazz_up',
        method: 'post',
        data,
    });
}
