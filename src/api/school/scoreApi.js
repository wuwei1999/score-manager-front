import request from '@/utils/request';


// 新增成绩
export function addScore(data) {
    return request({
        url: '/school/score',
        method: 'post',
        data,
    });
}

// 删除考试试卷
export function deleteExamPaper(data) {
    return request({
        url: '/school/score/delete_exam_paper',
        method: 'post',
        data,
    });
}

// 批量新增成绩
export function batchAddScore(data) {
    return request({
        url: '/school/score/batch',
        method: 'post',
        data,
    });
}

// 查询成绩列表
export function listScore(query) {
    return request({
        url: '/school/score/list',
        method: 'get',
        params: query,
    });
}

// 查询成绩列表(不分页)
export function listAllScore(query) {
    return request({
        url: '/school/score/all_list',
        method: 'get',
        params: query,
    });
}

//查询成绩图表数据
export function listEchartsData(query) {
    return request({
        url: '/school/score/echarts_list',
        method: 'get',
        params: query,
    });
}

//查询成绩图表数据
export function listAllExamPaper(query) {
    return request({
        url: '/school/score/exam_paper_list',
        method: 'get',
        params: query,
    });
}

// 删除成绩
export function deleteScore(scoreId) {
    return request({
        url: `/school/score/${scoreId}`,
        method: 'delete',
    });
}


// 考试试卷上传
export function uploadExamPaper(data) {
    return request({
        url: '/school/score/upload',
        headers: {
            'Content-Type': 'multipart/form-data',
        },
        method: 'post',
        data,
    });
}
