import request from '@/utils/request';

// 查询年级列表
export function listGrade(query) {
  return request({
    url: '/school/grade/list',
    method: 'get',
    params: query,
  });
}

// 查询年级详细
export function getGrade(gradeId) {
  return request({
    url: `/school/grade/${gradeId}`,
    method: 'get',
  });
}

// 新增年级
export function addGrade(data) {
  return request({
    url: '/school/grade',
    method: 'post',
    data,
  });
}

// 修改年级
export function updateGrade(data) {
  return request({
    url: '/school/grade',
    method: 'put',
    data,
  });
}

// 删除年级
export function deleteGrade(gradeId) {
  return request({
    url: `/school/grade/${gradeId}`,
    method: 'delete',
  });
}
