import request from '@/utils/request';

// 查询班级列表
export function listClazz(query) {
  return request({
    url: '/school/clazz/list',
    method: 'get',
    params: query,
  });
}

// 查询班级自身详细
export function getClazz(clazzId) {
  return request({
    url: `/school/clazz/${clazzId}`,
    method: 'get',
  });
}

// 查询班级周边详细
export function viewClazz(clazzId) {
  return request({
    url: `/school/clazz/view/${clazzId}`,
    method: 'get',
  });
}

// 新增班级
export function addClazz(data) {
  return request({
    url: '/school/clazz',
    method: 'post',
    data,
  });
}

// 修改班级
export function updateClazz(data) {
  return request({
    url: '/school/clazz',
    method: 'put',
    data,
  });
}

// 删除班级
export function deleteClazz(clazzId) {
  return request({
    url: `/school/clazz/${clazzId}`,
    method: 'delete',
  });
}
