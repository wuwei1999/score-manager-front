import request from '@/utils/request';

// 新增家长学生关系
export function addFamilyStudent(data) {
    return request({
        url: '/school/family_student',
        method: 'post',
        data,
    });
}
// 查询家长学生关系列表
export function listFamilyStudent(query) {
    return request({
        url: '/school/family_student/list',
        method: 'get',
        params: query,
    });
}

// 删除家长学生关系
export function deleteFamilyStudent(familyStudentId) {
    return request({
        url: `/school/family_student/${familyStudentId}`,
        method: 'delete',
    });
}
