import request from '@/utils/request';

// 查询知识点列表
export function listKnowledge(query) {
  return request({
    url: '/school/knowledge/list',
    method: 'get',
    params: query,
  });
}

// 查询知识点详细
export function getKnowledge(knowledgeId) {
  return request({
    url: `/school/knowledge/${knowledgeId}`,
    method: 'get',
  });
}

// 新增知识点
export function addKnowledge(data) {
  return request({
    url: '/school/knowledge',
    method: 'post',
    data,
  });
}

// 修改知识点
export function updateKnowledge(data) {
  return request({
    url: '/school/knowledge',
    method: 'put',
    data,
  });
}

// 删除知识点
export function deleteKnowledge(knowledgeId) {
  return request({
    url: `/school/knowledge/${knowledgeId}`,
    method: 'delete',
  });
}
