import request from '@/utils/request';

// 查询家长列表
export function listFamily(query) {
  return request({
    url: '/school/family/list',
    method: 'get',
    params: query,
  });
}

// 查询家长详细
export function getFamily(familyId) {
  return request({
    url: `/school/family/${familyId}`,
    method: 'get',
  });
}

// 新增家长
export function addFamily(data) {
  return request({
    url: '/school/family',
    method: 'post',
    data,
  });
}

// 修改家长
export function updateFamily(data) {
  return request({
    url: '/school/family',
    method: 'put',
    data,
  });
}

// 删除家长
export function deleteFamily(familyId) {
  return request({
    url: `/school/family/${familyId}`,
    method: 'delete',
  });
}

// 用户密码重置
export function resetFamilyPassword(familyId, password) {
  const data = {
    familyId,
    password,
  };
  return request({
    url: `/school/family/${familyId}/password/reset`,
    method: 'put',
    data,
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/file/upload',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    method: 'post',
    data,
  });
}

