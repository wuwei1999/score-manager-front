import request from '@/utils/request';

// 查询班级课程列表
export function listClazzCourse(query) {
  return request({
    url: '/school/clazz_course/list',
    method: 'get',
    params: query,
  });
}

// 查询班级课程详细
export function getClazzCourse(clazzCourseId) {
  return request({
    url: `/school/clazz_course/${clazzCourseId}`,
    method: 'get',
  });
}

// 新增班级课程
export function addClazzCourse(data) {
  return request({
    url: '/school/clazz_course',
    method: 'post',
    data,
  });
}

// 修改班级课程
export function updateClazzCourse(data) {
  return request({
    url: '/school/clazz_course',
    method: 'put',
    data,
  });
}

// 删除班级课程
export function deleteClazzCourse(clazzCourseId) {
  return request({
    url: `/school/clazz_course/${clazzCourseId}`,
    method: 'delete',
  });
}
