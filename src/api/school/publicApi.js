import request from '@/utils/request';

// 首页数据
export function indexData() {
    return request({
        url: '/school/public/indexData',
        method: 'get',
    });
}

// 查询学生年度列表
export function studentYearList() {
    return request({
        url: '/school/public/student_year_list',
        method: 'get',
    });
}

// 查询年级列表
export function allGradeList() {
    return request({
        url: '/school/public/grade_all_list',
        method: 'get',
    });
}

// 查询课程列表
export function allCourseList() {
    return request({
        url: '/school/public/course_all_list',
        method: 'get',
    });
}

// 查询年级课程列表
export function allGradeCourseList() {
    return request({
        url: '/school/public/grade_course_all_list',
        method: 'get',
    });
}

// 查询考试列表
export function allExamList() {
    return request({
        url: '/school/public/exam_all_list',
        method: 'get',
    });
}
// 查询教师列表
export function allTeacherList() {
    return request({
        url: '/school/public/teacher_all_list',
        method: 'get',
    });
}

// 查询班级列表
export function allClazzList(query) {
    return request({
        url: '/school/public/clazz_all_list',
        method: 'get',
        params: query,
    });
}

// 查询知识点列表
export function allKnowledgeList(query) {
    return request({
        url: '/school/public/knowledge_all_list',
        method: 'get',
        params: query,
    });
}

// 查询学生列表
export function allStudentList(query) {
    return request({
        url: '/school/public/student_all_list',
        method: 'get',
        params: query,
    });
}
