import request from '@/utils/request';

// 查询年级课程列表
export function listGradeCourse(query) {
  return request({
    url: '/school/grade_course/list',
    method: 'get',
    params: query,
  });
}

// 查询年级课程详细
export function getGradeCourse(gradeCourseId) {
  return request({
    url: `/school/grade_course/${gradeCourseId}`,
    method: 'get',
  });
}

// 新增年级课程
export function addGradeCourse(data) {
  return request({
    url: '/school/grade_course',
    method: 'post',
    data,
  });
}

// 修改年级课程
export function updateGradeCourse(data) {
  return request({
    url: '/school/grade_course',
    method: 'put',
    data,
  });
}

// 删除年级课程
export function deleteGradeCourse(gradeCourseId) {
  return request({
    url: `/school/grade_course/${gradeCourseId}`,
    method: 'delete',
  });
}
