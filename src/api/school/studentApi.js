import request from '@/utils/request';

// 查询学生列表
export function listStudent(query) {
  return request({
    url: '/school/student/list',
    method: 'get',
    params: query,
  });
}

// 查询学生详细
export function getStudent(studentId) {
  return request({
    url: `/school/student/${studentId}`,
    method: 'get',
  });
}

// 新增学生
export function addStudent(data) {
  return request({
    url: '/school/student',
    method: 'post',
    data,
  });
}

// 修改学生
export function updateStudent(data) {
  return request({
    url: '/school/student',
    method: 'put',
    data,
  });
}

// 删除学生
export function deleteStudent(studentId) {
  return request({
    url: `/school/student/${studentId}`,
    method: 'delete',
  });
}

// 用户密码重置
export function resetStudentPassword(studentId, password) {
  const data = {
    studentId,
    password,
  };
  return request({
    url: `/school/student/${studentId}/password/reset`,
    method: 'put',
    data,
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/file/upload',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    method: 'post',
    data,
  });
}
